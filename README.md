# JEXCHANGE.IO

Commercial website of [JEXchange.io](jexchange.io)

## Build & export app

```
npm install
npm run build
npm run export
```

Copy/past all files and directory of /out in your /www server repo


## Change theme colors

- [ ] [Generate Tailwind Shades](https://www.tailwindshades.com/)
- [ ] Replace values in tailwind.config.js by the new one


## Authors
Stefano Martines
