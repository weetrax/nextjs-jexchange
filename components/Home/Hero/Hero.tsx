import PropTypes from 'prop-types';
import React from 'react';
import SVGExchange from './SVGExchange';

type HeroProps = {
  //
};

const Hero: React.FC<HeroProps> = () => {
  return <div className="container mx-auto px-3 flex min-h-screen items-center justify-center">
    <div className="-mt-8 space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10 items-center justify-between w-full">
      <div className="text-center md:text-left">
        <h1 className='font-extrabold text-4xl sm:text-5xl lg:text-6xl tracking-tight'>
          <strong><span className='text-primary-500'>JEX</span>CHANGE.IO</strong>
        </h1>
        <h2 className="tracking-tight text-xl">
          A P2P decentralized exchange built on Elrond
        </h2>
        <div className='mt-4 flex justify-center md:justify-start gap-2'>
          <a rel="noreferrer" target={"_blank"} className='flex items-center px-3 py-2 bg-primary-500 border border-primary-500 hover:bg-primary-400 rounded text-base transition-all duration-200 ease-in-out hover:border-primary-500' href='https://app.jexchange.io/'>APP</a>
          <a rel="noreferrer" target={"_blank"} className='flex items-center px-3 py-2 border border-white rounded text-base transition-all duration-200 ease-in-out hover:border-primary-500' href='https://medium.com/@jexchangep2p/jex-litepaper-b5d48e69b084'>LITEPAPER (EN)</a>
          <a rel="noreferrer" target={"_blank"} className='flex items-center px-3 py-2 border border-white rounded text-base transition-all duration-200 ease-in-out hover:border-primary-500' href='https://medium.com/@jexchangep2p/jex-litepaper-11c5a168d1dc'>LITEPAPER (FR)</a>
        </div>
      </div>
      <div>
        <div className='w-1/2 md:w-4/5 mx-auto'>
          <SVGExchange />
        </div>
      </div>
    </div>
  </div>
};

Hero.propTypes = {
  //
};

export default Hero;
