import PropTypes from 'prop-types';
import React from 'react';
import Typed from 'react-typed';

type FootProps = {
  //
};

const Foot: React.FC<FootProps> = () => {
  return <div className='container mx-auto px-3 py-16'>
    <div className='text-center'>
      <p className="mt-2 text-3xl leading-8 font-extrabold tracking-tight sm:text-4xl">Start {` `}
        <span className='text-primary-500'>
          <Typed
            strings={[
              'trading',
              'earning',
              'staking']}
            typeSpeed={100}
            backSpeed={100}
            loop >
            <span></span>
          </Typed>
        </span> now !</p>
      <p className="mt-4 max-w-2xl text-xl mx-auto">
        Connect your crypto wallet to start using the app in seconds.
      </p>
      <p className='text-sm italic'>
        No registration needed.
      </p>
      <div className='py-4'>
        <a rel="noreferrer" target={"_blank"} className='px-3 py-2 bg-primary-500 border border-primary-500 hover:bg-primary-400 rounded text-lg transition-all duration-200 ease-in-out hover:border-primary-500' href='https://app.jexchange.io/'>Launch APP</a>
      </div>
    </div>
  </div>
};

Foot.propTypes = {
  //
};

export default Foot;
