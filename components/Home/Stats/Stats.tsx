import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { numberWithCommas } from '../../../utils';

type StatsProps = {
  //
};

type Price = {
  rate: number,
  unit: string
}

const initalJexHolderState = 3300;

const Stats: React.FC<StatsProps> = () => {

  useEffect(() => {
    fetch('https://microservice.jexchange.io/prices/JEX-9040ca').then(async (response) => {
      const res = await response.json() as Price;
      const fix = (res.rate as number).toFixed(8)
      setJexPrice({...res, rate: parseFloat(fix)})
    })

    fetch('https://api.elrond.com/tokens/JEX-9040ca/accounts/count').then(async (response) => {
      const res = await response.json();
      setJexHolders(res)
    })
  }, [])

  const [jexPrice, setJexPrice] = useState<Price | null>(null)
  const [jexHolders, setJexHolders] = useState<number>(initalJexHolderState);

  return <>
    <svg xmlns="http://www.w3.org/2000/svg" className="-mt-36" viewBox="0 0 1440 320"><path fill="#00a19a" fillOpacity="1" d="M0,64L48,90.7C96,117,192,171,288,202.7C384,235,480,245,576,256C672,267,768,277,864,245.3C960,213,1056,139,1152,117.3C1248,96,1344,128,1392,144L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>
    <div className='bg-primary-500'>
      <div className='container mx-auto px-3 pt-12'>
        <div className='grid grid-cols-4 items-center justify-center text-center gap-12 text-dark-500'>
          <div className='col-span-2 md:col-span-1'>
            <div className='flex gap-2 flex-col'>
              <div className='font-extrabold text-3xl sm:text-4xl lg:text-5xl tracking-tight'>
                $203M+
              </div>
              <div className='text-xs tracking-tight'>
                Trade Volume
              </div>
            </div>
          </div>
          <div className='col-span-2 md:col-span-1'>
            <div className='flex gap-2 flex-col'>
              <div className='font-extrabold text-3xl sm:text-4xl lg:text-5xl tracking-tight'>
                {numberWithCommas(jexHolders)}+
              </div>
              <div className='text-xs tracking-tight'>
                JEX Holders
              </div>
            </div>
          </div>
          <div className='col-span-2 md:col-span-1'>
            <div className='flex gap-2 flex-col'>
              <div className='font-extrabold text-3xl sm:text-4xl lg:text-5xl tracking-tight'>
                {numberWithCommas(6477)}+
              </div>
              <div className='text-xs tracking-tight'>
                Filled offers
              </div>
            </div>
          </div>
          <div className='col-span-2 md:col-span-1'>
            <div className='flex gap-2 flex-col'>
              <div className='font-extrabold text-3xl sm:text-4xl lg:text-5xl tracking-tight'>
                17+
              </div>
              <div className='text-xs tracking-tight'>
                Token listed
              </div>
            </div>
          </div>
          {
            jexPrice && <div className="col-span-4">
              <div className='font-extrabold text-3xl sm:text-4xl lg:text-5xl tracking-tight'>
                {jexPrice?.rate} / {jexPrice?.unit}
              </div>
              <div className='text-xs tracking-tight'>
                $JEX Price
              </div>
            </div>
          }
        </div>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#00a19a" fillOpacity="1" d="M0,192L48,192C96,192,192,192,288,213.3C384,235,480,277,576,250.7C672,224,768,128,864,117.3C960,107,1056,181,1152,208C1248,235,1344,213,1392,202.7L1440,192L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path></svg>
  </>
};

Stats.propTypes = {
  //
};

export default Stats;
