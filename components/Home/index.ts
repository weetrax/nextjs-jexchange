export { default as Features } from "./Features";
export { default as Stats } from "./Stats";
export { default as Foot } from "./Foot";
export { default as Hero } from "./Hero";