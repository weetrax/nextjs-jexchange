import Footer from '../components/Footer';
import Head from 'next/head';
import Navbar from '../components/Navbar';
import type { NextPage } from 'next'
import Contact from '../components/Contact';

const FAQPage: NextPage = () => {
  return (
    <div>
      <Head>
        <title>JEXchange.io - Contact us</title>
        <meta name="description" content="A P2P decentralized exchange built on Elrond" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Navbar />
        <div className='min-h-screen items-center justify-center'>
            <Contact />
        </div>
      </main>
      <Footer />
    </div>
  )
}

export default FAQPage
