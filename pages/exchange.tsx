import { Fragment } from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import { Features, Hero, Stats } from '../components/Home'
import Footer from '../components/Footer'
import { faCheck, faChevronDown, faCoins, faExchange, faMoneyBill, faPiggyBank, faSackDollar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { classNames } from '../utils';


type Section = "market" | "ido" | "stacking" | "analytics" | "offers" | "home"

const tokens = [
    {
        id: 1,
        name: 'Wade Cooper',
        avatar:
            'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 2,
        name: 'Arlene Mccoy',
        avatar:
            'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 3,
        name: 'Devon Webb',
        avatar:
            'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80',
    },
    {
        id: 4,
        name: 'Tom Cook',
        avatar:
            'https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 5,
        name: 'Tanya Fox',
        avatar:
            'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 6,
        name: 'Hellen Schmidt',
        avatar:
            'https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 7,
        name: 'Caroline Schultz',
        avatar:
            'https://images.unsplash.com/photo-1568409938619-12e139227838?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 8,
        name: 'Mason Heaney',
        avatar:
            'https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 9,
        name: 'Claudie Smitham',
        avatar:
            'https://images.unsplash.com/photo-1584486520270-19eca1efcce5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
    {
        id: 10,
        name: 'Emil Schaefer',
        avatar:
            'https://images.unsplash.com/photo-1561505457-3bcad021f8ee?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    },
]

const Home: NextPage = () => {

    const [selectedSection, setSelectedSection] = useState<Section>("market")
    const [selectedToken, setSelectedToken] = useState(tokens[0])

    const [stackingExtended, setStackingExtended] = useState(false)

    return (
        <div>
            <Head>
                <title>JEXchange.io - Exchange</title>
                <meta name="description" content="A P2P decentralized exchange built on Elrond" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <div className='bg-dark-500 shadow-md'>
                    <div className='container mx-auto p-3 flex flex-col gap-4'>
                        <div className='flex justify-between items-center text-sm'>
                            <a className='font-extrabold'><span className='text-primary-500'>JEX</span>CHANGE</a>
                            <div className='flex gap-2'>
                                <button className='bg-dark-500 rounded px-3 py-2 flex items-center gap-2'>
                                    <FontAwesomeIcon className='w-4' icon={faCoins} /> <span>$0.0399</span>
                                </button>
                                <a className='bg-primary-500 rounded px-3 py-2'>Wallet</a>
                            </div>
                        </div>
                        <div className='justify-center w-full lg:w-2/3 xl:1/2 mx-auto text-xs md:text-sm'>
                            <div className='rounded-full bg-dark-500 border border-primary-500 flex text-center gap-4 p-1'>
                                <button type="button" onClick={() => setSelectedSection("home")} className={`w-full p-2 ${selectedSection === "home" && "bg-primary-400"} rounded-full`}>Home</button>
                                <button type="button" onClick={() => setSelectedSection("market")} className={`w-full p-2 ${selectedSection === "market" && "bg-primary-400"} rounded-full`}>Market</button>
                                <button type="button" onClick={() => setSelectedSection("ido")} className={`w-full p-2 ${selectedSection === "ido" && "bg-primary-400"} rounded-full`}>IDO</button>
                                <button type="button" onClick={() => setSelectedSection("stacking")} className={`w-full p-2 ${selectedSection === "stacking" && "bg-primary-400"} rounded-full`}> Stacking</button>
                                <button type="button" onClick={() => setSelectedSection("analytics")} className={`w-full p-2 ${selectedSection === "analytics" && "bg-primary-400"} rounded-full`}> Analytics</button>
                                <button type="button" onClick={() => setSelectedSection("offers")} className={`w-full p-2 rounded-full`}> My offers</button>
                                <button type="button" className='w-full p-2 rounded-full'>...</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='min-h-screen'>
                    {
                        selectedSection === "home" && <>
                            <Hero />
                            <Stats />
                            <Features />
                        </>
                    }
                    {
                        selectedSection === "stacking" && <>
                            <div className='container mx-auto px-3 py-16'>
                                <div className='bg-dark-light rounded-lg px-2 py-4'>
                                    <div className='flex flex-col md:flex-row items-center justify-between gap-4'>
                                        <div className='flex items-center w-full md:w-auto'>
                                            <div className='flex items-center justify-between md:justify-start'>
                                                <div className="mr-3">
                                                    <div className="w-8">
                                                        <img src="/favicon.ico" alt="RIDE-7d18e9" className="token-symbol w-8" />
                                                    </div>
                                                </div>
                                                <div className="flex flex-col">
                                                    <div className="flex items-center font-bold lg:mb-1" data-testid="farmName">
                                                        <div className="flex">
                                                            <div className="h6 mb-0 font-semibold">Staked&nbsp;JEX</div>
                                                        </div>
                                                    </div>
                                                    <div className="flex items-center">
                                                        <div className="">
                                                            <span>102,621,640</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button onClick={() => setStackingExtended(!stackingExtended)} className='flex justify-end md:hidden w-full'>
                                                <FontAwesomeIcon icon={faChevronDown} className={`w-3 ${stackingExtended && "rotate-180"}`} />
                                            </button>
                                        </div>
                                        <div className='flex flex-row md:flex-col w-full md:w-auto justify-between'>
                                            <div className='font-semibold'>
                                                Max APR / APY
                                            </div>
                                            <div>
                                                9%
                                            </div>
                                        </div>
                                        <div className='flex flex-row md:flex-col w-full md:w-auto justify-between'>
                                            <div className='font-semibold'>
                                                My Stacked JEX
                                            </div>
                                            <div>
                                                21,356 ≈ <span className='text-primary-500'>
                                                    $12$
                                                </span>
                                            </div>
                                        </div>
                                        <div className='flex flex-col w-full md:w-auto gap-2'>
                                            <div className='text-sm p-1 bg-primary-500 rounded flex items-center justify-center'>
                                                Deposit
                                            </div>
                                            <div className='text-sm p-1 bg-primary-500 rounded flex items-center justify-center'>
                                                Withdraw
                                            </div>
                                            <div className='text-sm p-1 bg-primary-100 text-primary-900 rounded flex items-center justify-center'>
                                                Claim
                                            </div>
                                        </div>
                                        <button onClick={() => setStackingExtended(!stackingExtended)} className='hidden md:flex flex-col w-full md:w-auto gap-2'>
                                            <FontAwesomeIcon icon={faChevronDown} className={`w-3 ${stackingExtended && "rotate-180"}`} />
                                        </button>
                                    </div>
                                    <div className='py-6'>
                                        <div className='bg-dark-500 rounded-full flex'>
                                            <div className='flex bg-primary-500 h-2 rounded-l-full' style={{ width: "50%" }}>
                                                <div className='mt-3 text-xs'>Accumulation days - 11/21</div>
                                            </div>
                                            <div className='bg-white h-2 w-full border border-white' style={{ width: "30%" }}>

                                            </div>
                                            <div className='bg-white h-2 w-full rounded-r-full border-l border-l-black' style={{ width: "20%" }}>
                                                <div className='mt-3 text-xs'>Claiming period</div>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        stackingExtended &&

                                        <div className='pt-8'>
                                            <div className='flex justify-center gap-4 md:gap-20'>
                                                <div>
                                                    <div className='font-semibold flex gap-2 items-center'>
                                                        <i><FontAwesomeIcon icon={faCoins} className="w-5" /></i>
                                                        <span>
                                                            Tokens
                                                        </span>
                                                    </div>
                                                    <div className='pt-4 flex gap-4 flex-col'>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                BSK
                                                            </span>
                                                        </div>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                FIRE
                                                            </span>
                                                        </div>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                ITHEUM
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className='font-semibold flex gap-2 items-center'>
                                                        <i><FontAwesomeIcon icon={faPiggyBank} className="w-5" /></i>
                                                        <span>
                                                            My Stacking rewards
                                                        </span>
                                                    </div>
                                                    <div className='pt-4 flex gap-4 flex-col'>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                152.05  <span className='text-xs'>≈ $ 0.03</span>
                                                            </span>
                                                        </div>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                68.77  <span className='text-xs'>≈ $ 0.023</span>
                                                            </span>
                                                        </div>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                22.28  <span className='text-xs'>≈ $ 1.01</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className='font-semibold flex gap-2 items-center'>
                                                        <i><FontAwesomeIcon icon={faSackDollar} className="w-5" /></i>
                                                        <span>
                                                            Total Stacking rewards
                                                        </span>
                                                    </div>
                                                    <div className='pt-4 flex gap-4 flex-col'>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                27,152.05  <span className='text-xs'>≈ $ 1.33</span>
                                                            </span>
                                                        </div>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                1,368.77  <span className='text-xs'>≈ $ 1.53</span>
                                                            </span>
                                                        </div>
                                                        <div className='flex gap-6 items-center'>
                                                            <span>
                                                                16,222.28  <span className='text-xs'>≈ $ 13,461.01</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </>
                    }
                </div>
            </main>
            <Footer />
        </div>
    )
}

export default Home
