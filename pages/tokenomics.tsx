import Footer from '../components/Footer';
import Head from 'next/head';
import Navbar from '../components/Navbar';
import type { NextPage } from 'next'
import Tokenomics from '../components/Tokenomics';

const TokenomicsPage: NextPage = () => {
  return (
    <div>
      <Head>
        <title>JEXchange.io - Tokenomics</title>
        <meta name="description" content="A P2P decentralized exchange built on Elrond" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Navbar />
        <Tokenomics />
      </main>
      <Footer />
    </div>
  )
}

export default TokenomicsPage
