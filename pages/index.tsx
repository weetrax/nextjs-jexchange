import type { NextPage } from 'next'
import Navbar from '../components/Navbar'
import Head from 'next/head'
import { Hero, Stats, Features, Foot } from '../components/Home'
import Contact from '../components/Contact'
import Footer from '../components/Footer'

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>JEXchange.io</title>
        <meta name="description" content="A P2P decentralized exchange built on Elrond" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Navbar />
        <Hero />
        <Stats />
        <Features />
        <Contact />
        <Foot />
      </main>
      <Footer />
    </div>
  )
}

export default Home
